
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes React and other helpers. It's a great starting point while
 * building robust, powerful web applications using React + Laravel.
 */

require('./bootstrap');

import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import { createStore, combineReducers, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';
import MainApp from './MainApp';

// Import store reducers
import counterReducer from './store/reducers/counter';
import resultReducer from './store/reducers/result';

const rootReducer = combineReducers({
    counter: counterReducer,
    result: resultReducer
});

// Set up redux store
const store = createStore(rootReducer, composeWithDevTools(
    applyMiddleware(thunk)
));

// Set up app with store and browserrouter
const app = (
    <Provider store={store}>
        <BrowserRouter>
            <MainApp />
        </BrowserRouter>
    </Provider>
)

ReactDOM.render(app, document.getElementById('root'));