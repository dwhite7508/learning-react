export const INCREMENT_COUNTER = 'INCREMENT_COUNTER';
export const ADD_TO_COUNTER = 'ADD_TO_COUNTER';
export const ADD_NUMBER_TO_COUNTER = 'ADD_NUMBER_TO_COUNTER';
export const STORE_RESULT = 'STORE_RESULT';
export const DELETE_RESULT = 'DELETE_RESULT';
