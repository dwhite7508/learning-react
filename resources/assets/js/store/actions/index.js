export {
    increment,
    addToCounter,
    addNumberToCounter
} from './counter';

export {
    storeResult,
    deleteResult
} from './result';