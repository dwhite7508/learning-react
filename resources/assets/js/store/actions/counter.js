import * as actionTypes from './actionTypes'

export const increment = () => {
    return {
        type: actionTypes.INCREMENT_COUNTER
    };
};

export const addToCounter = (num) => {
    return {
        type: actionTypes.ADD_TO_COUNTER,
        number: num
    };
};

// ...that gets called by an asynchronous function
export const addNumberToCounter = (number) => dispatch => {
    dispatch({
        type: actionTypes.ADD_NUMBER_TO_COUNTER,
        number: number
    })
    return Promise.resolve();
}