import * as actionTypes from './actionTypes'

// Write a synchronous function...
export const saveResult = (res) => {
    return {
        type: actionTypes.STORE_RESULT,
        result: res
    };
};

// ...that gets called by an asynchronous function
export const storeResult = (res) => {
    return (dispatch, getState) => {
        const oldCounter = getState().counter.counter;
        console.log(oldCounter);
        setTimeout(() => {
            dispatch(saveResult(res));
        }, 2000);
    };
};

export const deleteResult = (resultElId) => {
    return {
        type: actionTypes.DELETE_RESULT,
        resultElId: resultElId
    };
};