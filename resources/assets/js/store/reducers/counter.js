import * as actionTypes from '../actions/actionTypes';

const initialState = {
    counter: 8
}

const reducer = (state = initialState, action) => {

    // Set up ALL the action types and what to do with them
    switch (action.type) {
        case actionTypes.INCREMENT_COUNTER:
            return {
                ...state,
                counter: state.counter + 1
            }
        case actionTypes.ADD_TO_COUNTER:
            return {
                ...state,
                counter: state.counter + action.number
            }
        case actionTypes.ADD_NUMBER_TO_COUNTER:
            return {
                ...state,
                counter: parseInt(state.counter) + parseInt(action.number)
            }

    };

    return state;
}

export default reducer;