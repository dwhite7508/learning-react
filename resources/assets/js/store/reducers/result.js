import * as actionTypes from '../actions/actionTypes';

const initialState = {
    results: []
}

const reducer = (state = initialState, action) => {

    // Set up ALL the action types and what to do with them
    switch (action.type) {
        case actionTypes.STORE_RESULT:
            return {
                ...state,
                // Use concat to immutably add to arrays in state
                results: state.results.concat({ id: (new Date().getTime()).toString(16), value: action.result })
            }
        case actionTypes.DELETE_RESULT:
            const updatedArray = state.results.filter((result) => result.id !== action.resultElId);
            return {
                ...state,
                results: updatedArray
            }

    };

    return state;
}

export default reducer;