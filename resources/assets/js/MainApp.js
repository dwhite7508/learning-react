import React, { Component } from 'react';
import { Route, Switch, Link } from 'react-router-dom';

// Import Route components
import Homepage from './containers/Homepage/Homepage';
import AboutUs from './containers/AboutUs/AboutUs';
import AboutUsNext from './containers/AboutUs/AboutUsNext';

class MainApp extends Component {
    render() {
        return (
            <div>
                <header>
                    <ul>
                        <li><Link to="/">Home</Link></li>
                        <li><Link to="/about-us?name=daniel">About Us</Link></li>
                        <li><Link to="/about-us/next">About Us (More)</Link></li>
                    </ul>
                </header>
                <Switch>
                    <Route exact path="/about-us" component={AboutUs} />
                    <Route exact path="/about-us/next" component={AboutUsNext} />
                    <Route exact path="/" component={Homepage} />
                </Switch>
            </div>
        );
    }
}

export default MainApp;