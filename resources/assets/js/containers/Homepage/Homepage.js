import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actionCreators from '../../store/actions/index'
import Aux from '../../hoc/Aux';

class Homepage extends Component {

    state = {
        counterValue: 0
    }

    valueChangedHandler = (event) => {
        this.setState({ counterValue: event.target.value });
    }

    render() {
        return (
            <Aux>
                <h1>Homepage!</h1>
                <p>Counter Value = {this.props.ctr}</p>
                <button onClick={this.props.onIncrementCounter}>Increment Counter</button><br />
                <button onClick={this.props.onAddToCounter}>Add 10 to the Counter</button>
                <hr />
                <button onClick={() => this.props.onStoreResult(this.props.ctr)}>Store Result</button>
                <hr />
                <input type="text"
                    placeholder="Add Number to Counter"
                    onChange={this.valueChangedHandler}
                    value={this.state.counterValue}
                />
                <button onClick={() => this.props.onAddNumberToCounter(this.state.counterValue).then(() => { this.setState({ counterValue: 0 }) })}>Add to Counter</button>
                <ul>
                    {this.props.storedResults.map(strResult => (
                        <li key={strResult.id} onClick={() => this.props.onDeleteResult(strResult.id)}>{strResult.value}</li>
                    ))}
                </ul>
            </Aux>
        )
    }
}

// Need to pass what actions and state items you want to "connect"
const mapStateToProps = state => {
    return {
        ctr: state.counter.counter,
        storedResults: state.result.results
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onIncrementCounter: () => dispatch(actionCreators.increment()),
        onAddToCounter: () => dispatch(actionCreators.addToCounter(10)),
        onAddNumberToCounter: (number) => dispatch(actionCreators.addNumberToCounter(number)),
        onStoreResult: (result) => dispatch(actionCreators.storeResult(result)),
        onDeleteResult: (id) => dispatch(actionCreators.deleteResult(id))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Homepage);