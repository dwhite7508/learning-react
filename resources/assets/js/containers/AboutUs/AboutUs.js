import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import queryString from 'query-string';
import Aux from '../../hoc/Aux';

class AboutUs extends Component {

    state = {
        totalPrice: 4
    }

    render() {

        const qstring = queryString.parse(location.search);

        console.log(this.props.match.params);

        let welcomeString = 'Welcome to the about us page.';

        if (qstring.name) {
            welcomeString = 'Welcome to the about us page ' + qstring.name;
        }

        return (
            <Aux>
                <h1>About Us!</h1>
                <p>{welcomeString}</p>
                <Link to="/about-us/next">More about us &raquo;</Link>
            </Aux>
        )
    }
}

export default AboutUs;